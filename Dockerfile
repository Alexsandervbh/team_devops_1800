FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y \
    software-properties-common \
    sshpass \
    python3-pip

RUN pip3 install ansible

COPY ansible.cfg /etc/ansible/ansible.cfg

WORKDIR /ansible

CMD ["/bin/bash"]
